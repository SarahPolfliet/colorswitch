﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;

public class Player : MonoBehaviour
{
    private Rigidbody2D rb;
    private SpriteRenderer sr;
    private Dictionary<String, Color> colorMap = new Dictionary<String, Color>()
    {
        {"Blue", new Color(53f/255f, 226f/255f, 242f/255f)},
        {"Yellow", new Color(246f/255f, 223f/255f, 14f/255f)},
        {"Pink", new Color(255f/255f, 0f/255f, 128f/255f)},
        {"Violet", new Color(140f/255f, 19f/255f, 251f/255f)}
    };

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        GetRandomColor();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.velocity = Vector3.up * 5;
        }
    }

    void OnTriggerEnter2D(Collider2D col) 
    {
        if (col.tag == "ChangeColor")
        {
            GetRandomColor();
        } 
        else if (col.tag == "Finish")
        {
            FindObjectOfType<GameManager>().WinGame();
        }
        else if (col.tag != sr.tag)
        {
            FindObjectOfType<GameManager>().LoseGame();
        }
    }

    void GetRandomColor() 
    {
        System.Random rand = new System.Random();
        int index = rand.Next(colorMap.Count);
        String colorName = colorMap.Keys.ElementAt(index);
        Color color = GetColorByKey(colorName);

        sr.color = color;
        sr.tag = colorName;
    }

    Color GetColorByKey(String colorName)
    {
        return colorMap.FirstOrDefault(x => x.Key == colorName).Value;
    }
}
