﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public void WinGame()
    {
        //You won
        SceneManager.LoadScene(2);   
    }

    public void LoseGame()
    {
        //Game Over
        SceneManager.LoadScene(3); 
    }
}
